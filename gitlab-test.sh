#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
NC='\033[0m'


set -e

function checkYarn {
    echo -e "\n${GREEN}check yarn, nodejs, npm, and aws-cli:${NC}\n"
    echo "yarn version: $(yarn -v)"
    echo "nodejs version: $(node -v)"
    echo "npm version: $(npm -v)"
    echo $(aws --version)
}

checkYarn
