FROM alpine:latest

RUN addgroup -S builder && \
    adduser -S -D -H -G builder -s /sbin/nologin builder

COPY . /home/builder/

WORKDIR /home/builder/

RUN apk update && \
    apk add --purge --no-cache $(cat alpine-package.lst) &&\
    yarn global add $(cat yarn-package.lst) &&\
    pip install awscli &&\
    chown -R builder:builder /home/builder

USER builder
